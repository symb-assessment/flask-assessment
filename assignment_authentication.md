
# Overview

Create REST APIs to be consumed by a mobile app for storing contacts using mobile app.  

# Technology Stack

You need to use Flask for creating the API. And use MySQL or PostgreSQL as database.

# Terminology and Assumptions:

* User can signup into application using email, name and password.
* User can login into the application using email and password.
* Each registered user of the app can have zero or more personal "Contacts".
* The UI will be built by someone else - you are simply making the REST API endpoints to be
consumed by the front end.
* You will be writing the code as if it’s for production use and should thus have the required
performance and security.

# API Response Format

This will be response format for all the APIs, for both success and error

```json
{
    "message" : "Any message need to show to the client",
    "data": {

    }
}
```
- message: This will be any message need to show to the end user 
- data: This will be type of object, and have data returned by the API. If there is no data then this will have an empty object.

And proper error code is required for all the api responses

- 400 Bad Request: For any wrong data submitted through the API
- 401 Unauthorized: For all the unauthorized API calls
- 403 Forbidded: For all the forbidden api calls
- 200 Success: For all success API calls
- 500 Internal Server Error: For all unhandled errors (Avoid this status by handling most of the cases)

# Database

## User
Following information required for each user registered on the application
* Name
* Phone Number
* Email
* Address

## Contacts
Each user can store as many as contact in hi profile with following information
* Name
* Email
* Phone
* Address
* Country

Use SQLAlchemy relationship to relate the two tables [https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/#one-to-many-relationships](https://flask-sqlalchemy.palletsprojects.com/en/2.x/models/#one-to-many-relationships)

# APIs Required

## 1. API to signup using name, email and password.

Curl:

```bash
curl --location 'http://localhost:5001/user/singup' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Raj Kumar",
    "email": "raj@example.com",
    "password": "samplepassword"
}'
```

### Use Cases:

- Email will be the unique data for each row, is user with same email alaready exist into the database return a JSON response with error code 400

```json
{
    "message": "Email already registered",
    "data" : {}
}
```

- Store the password in encrypted form in the database
- Add validation for all the fields. If anything is wrong send JSON response with 400 

```json
{
    "message": "Name cannot be left blank",
    "data": {}
}
```

- Also add validation for valie email address, if the email is not valid return 400 error response

```json
{
    "message": "Email is not valid",
    "data": {}
}

- In case of successfull user signup send JSON respons with 200 code in this format:

```json
{
    "message" : "User signup complete",
    "data": {
        "access_token": "jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds",
        "user": {
            "id": 9,
            "name": "Raj Kumar",
            "email": "raj@example.com"
        }
    }
}
```

- Access token is a random generated token on successfull signup or login. This access token will be used in other protected APIs to identify the logged in user. Please learn more about access token from this URL: [https://auth0.com/docs/secure/tokens/access-tokens](https://auth0.com/docs/secure/tokens/access-tokens)
- user is the data of the new signned up user

## 2. API to login into the application

Curl:

```bash
curl --location 'http://localhost:5001/user/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "raj@example.com",
    "password": "samplepassword"
}'
```

### Use Cases:

- This API will verify the email and password for the login, and if the data (email and password) is not valie this API will return JSON error response with code 401, and the response will looks like

```json
{
    "message" : "Invalid credentials",
    "data": {}
}
```

- Add validation on both the fields and if any data is not valid the API will return JSON response with 400 code

```json
{
    "message": "Email is not valid",
    "data": {}
}
```

- If the email submitted through the api is not registered return JSON response with status code 400

```json
{
    "message": "Email not registered",
    "data": {}
}
```

- If the email and password are correct return success response with error code 200 or 201

```json
{
    "message" : "Login successful",
    "data": {
        "access_token": "jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds",
        "user": {
            "id": 9,
            "name": "Raj Kumar",
            "email": "raj@example.com"
        }
    }
}
```

## 3. API to get user's details

This api will return the logged in user detail using the access token passed in the API request header

Curl

```bash
curl --location 'http://localhost:5001/user' \
--header 'Authorization: Bearer jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds'
```

### Use Cases:

- If the access token passed is correct return the user detail with 200 status code

```json
{
    "message" : "User detail",
    "data": {
        "id": 9,
        "name": "Raj Kumar",
        "email": "raj@example.com"
    }
}
```

- If the access token is wrong return JSON response with 401 status code

```json
{
    "message": "Authentication failed",
    "data": {}
}
```

## 4. API to create new contact

This api will add new contact for the logged in user

Curl:

```bash
curl --location 'http://localhost:5001/contact' \
--header 'Authorization: Bearer jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "John Deo",
    "email": "john@example.com",
    "phone": "9909909902",
    "country": "England",
    "address": "12/90 Bakers Street, London, UK"
}'
```

User cases:

- Only name and phone field are mandatory, so in case name or phone is blank send error response with status code 400

```json
{
    "message": "Name is required",
    "data": {}
}
```

- In case of email is submitted but the format is not correct send a JSON response with 400 code
- In case contact added successfully, send 200 response with the newly added contact

```json
{
    "message": "Contact added",
    "data": {
        "id": 89,
        "name": "John Deo",
        "email": "john@example.com",
        "phone": "9909909902",
        "country": "England",
        "address": "12/90 Bakers Street, London, UK"
    }
}
```

The extra field 'id' is the ID of newly added contact in database 

## 5. API to list all the contacts

This API will list all the contacts of logged in user in paginated list

```bash
curl --location 'http://localhost:5001/contact?page=1' \
--header 'Authorization: Bearer jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds'
```

Request Query Parameter:

- Page (int): Return the ontact list based on this page number. If page is not specified in API, use 1 as default 

```
If page = 1, return recent 10 contacts. 
```

```
If page = 3, return recent 10 contacts, skipping 2 pages, that is contact from 21 to 30
```

- Limit (int): Return the number of records per page.
If limit is not specified in the API, use 10 as default limit



### Use Cases:

- The contact list will be in paginated form, all the APIs will return few paginated data, please find below the sample response

```json
{
    "message": "Contact list",
    "data": {
        "list": [],
        "has_next": true,
        "has_prev": false,
        "page": 1,
        "pages": 10,
        "per_page": 10,
        "total": 98
    }
}
```

- And refer to [Flask SqlAlchemy Paginate](https://flask-sqlalchemy.palletsprojects.com/en/3.0.x/api/#flask_sqlalchemy.SQLAlchemy.paginate) for pagination
- If there are no contacts, return 200 success response with no data in the 'list'

6. API to sort the contact by
    - Latest first
    - Oldest first
    - Alphabetically

This API is an extension of previous API with a new query parameter 'sort_by'. Sort by will sort the contacts based on the value of it. 

Possible values of sort_by:

- latest: Return the latest paginated contact list
- oldest: Return the oldest paginated contact list
- alphabetically_a_to_z: Return the paginated contact list sorted by alphabetically from A to Z
- alphabetically_z_to_a: Return the paginated contact list sorted by alphabetically from Z to A

Curl:

```bash
curl --location 'http://localhost:5001/contact?page=1&sort_by=latest' \
--header 'Authorization: Bearer jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsalksdfjlkds'
```

Response will be same a paginated contact list API


7. API to search a contact by:
    * Name
    * Email
    * Phone

This API is an extension of previous API with few more new query parameter name, email and phone. Based on the values of these parameters the list will be filtered

Parameters:

- name: If name is passed the contact list will be filtered by the name
- email: If email is passed the contact list will be filtered by the email
- phone: If phone is passed the contact list will be filtered by the phone

Curl:

```bash
curl --location 'http://localhost:5001/contact?page=1&sort_by=latest&limit=10&name=john&email=gmail.com&phone=99' \
--header 'Authorization: Bearer jkhrjkwehj245khk4j3h3kjhkj3hj4kj5h3k4jh5kljtrlkjnmdxnbxmnbdfsa;lksdfjlkds'
```

Response will be same a paginated contact list API




# Evaluation criteria:
* Completeness of functionality
* Correctness under thorough testing
* Performance and scalability of APIs
* Security of APIs
* Data modeling
* Structure of code
* Readability of code

# Deployment:
* Deploy the APIs on any of the public URL

# Assignment Submition:
* Push the assignment on Gitlab or Github and share the URL as submission
* Share the postman collection along with the Git URL
* Share the public URL of the application

